const { Component } = require('inferno');
const classname = require('hexo-component-inferno/lib/util/classname');
const Head = require('./common/head');
const Navbar = require('./common/navbar');
const Widgets = require('./common/widgets');
const Footer = require('./common/footer');
const Scripts = require('./common/scripts');
const Search = require('./common/search');



module.exports = class extends Component {
    render() {
        const { site, config, page, helper, body } = this.props;
        const { url_for } = helper;

        const language = page.lang || page.language || config.language;
        const columnCount = Widgets.getColumnCount(config.widgets);

        return <html lang={language ? language.substr(0, 2) : ''}>
            <Head site={site} config={config} helper={helper} page={page} />
            <body class={`is-${columnCount}-column`}>
                <Navbar config={config} helper={helper} page={page} />
                <div class="img-header" 
                    style={{
                            width: "100%",
                            "box-shadow": "0px 2px 5px rgba(0,0,0,6%)"
                    }}>
                    <picture>
                        <source media="(max-width: 900px)" srcset={url_for("/img/blog-header-m.png")}/>
                        <source media="(min-width: 900px)" srcset={url_for("/img/blog-header.png")}/>
                        <img src={url_for("/img/blog-header.png")} alt="blog-header" style="width:auto;" style={{ display: "block" }}/>
                    </picture>
                </div>
                <section class="section">
                    <div class="container">
                        <div class="columns">
                            <div class={classname({
                                column: true,
                                'order-2': true,
                                'column-main': true,
                                'is-12': columnCount === 1,
                                'is-8-tablet is-8-desktop is-9-widescreen': columnCount === 2,
                                'is-8-tablet is-8-desktop is-6-widescreen': columnCount === 3
                            })} dangerouslySetInnerHTML={{ __html: body }}></div>
                            <Widgets site={site} config={config} helper={helper} page={page} position={'left'} />
                            <Widgets site={site} config={config} helper={helper} page={page} position={'right'} />
                        </div>
                    </div>
                </section>
                <Footer config={config} helper={helper} />
                <Scripts site={site} config={config} helper={helper} page={page} />
                <Search config={config} helper={helper} />
            </body>
        </html>;
    }
};
