---
title: Blog Của Mình ???
date: 2021-06-06 21:33:46
tags: blog
categories:
- Giới thiệu
cover: /img/Blog-Cua-Minh/banner.jpg
thumbnail: /img/Blog-Cua-Minh/banner.jpg
---

Đúng rồi, không nhầm đâu, đây là blog mình mới tạo đó. Thực ra thì mình đã có dự định viết blog cách đây 3-4 tháng gì rồi, nhưng thực sự không có thời gian. Mãi đến hôm nay, mình mới có thể tạo được một *"đứa con"* này. Vậy thì...
<!-- more -->
...Welcome to My Blog <3

<center> 
 {%  img img-align-center /img/Blog-Cua-Minh/blog-la-gi.jpg 700 700 '"Blog image" "Credit: Adflex"'  %} 
</center>

# Tại sao mình lại tạo blog ??

Dòng đời đưa đẩy thôi...Chứ ai mà muốn :)

Đùa chứ thật ra thì nhiều khi mình muốn chia sẻ nhiều thứ hay ho lên cho mọi người ấy, khổ nỗi là facebook thì mình thấy nó không được "cá nhân" cho lắm với cả viết nếu dài thì nhìn sẽ rất là mệt (chữ không là chữ, không chèn hình ở giữa được). Do đó mình mới nảy sinh một suy nghĩ đó là *"Tại sao mình không thử làm một trang blog cho riêng mình nhỉ ?"*. 

Để rồi từ đó có một nơi mà mình có thể nói lên những điều mà mình muốn, những câu chuyện mà mình muốn kể, những kiến thức mà mình muốn người khác cùng đọc. Nhờ vậy mà những lúc buồn chán, mình đều có thể giải tỏa ra. Tin mình đi, mình không nói dối đâu, càng viết bạn sẽ càng cảm thấy nhẹ nhõm sau những lần bực tức, sau những lần mà tâm trạng bạn cảm thấy không được ổn. Thật tuyệt vời phải không :3

Ngoài ra cũng tiện cải thiện luôn cái vốn viết lách dở ẹt của mình. Hồi xưa còn là học sinh, mình làm văn chỉ toàn là 6-7đ đều thôi à, viết bài thì lúc nào cũng giáo viên nhận xét là sai chính tả, câu cú lủng củng, lặp từ nhiều,... nên mình không được thích môn văn cho lắm. Dù vậy, theo mình kỹ năng viết lách là một kỹ năng hoàn toàn cần thiết và mang lại rất nhiều lợi ích cho bạn bất kể là bạn theo hướng xã hội hay khoa học. Càng viết tốt sẽ càng chứng tỏ được bạn là một con người "đỉnh của đỉnh" (haha). Thôi lý do lý trấu đến đây được rồi, sang phần kế nha.

<center> 
 {%  img img-align-center /img/Blog-Cua-Minh/happy.jpg 700 700 '"happy image" "Credit: happierhuman.com"'  %} 
</center>

# Mình đã tạo ra blog này như thế nào ??

Rất là khó, tốn cả tháng mới xong !!!

Đó là suy nghĩ của mình trước khi bắt tay vào làm thôi. Thật sự mình thấy nhiều blog hoành tráng mà thấy mê, nhìn nó phức tạp lắm nên mình tưởng để tạo ra một blog cũng sẽ tốn khá nhiều thời gian đấy. Nhưng bạn tin được không ? Blog này mình tạo ra chỉ trong **2 ngày cuối** tuần của mình thôi (OMG). 

Làm rồi mới biết nó không hề khó, có rất rất nhiều nền tảng giúp bạn làm nóng ngay một cái blog cho bản thân như Blogger (dành cho người không có kiến thức về IT) hay Hexo, Hugo (dành cho những ai đã học qua về IT). Chính cái blog này cũng được mình tạo ra từ [Hexo](https://hexo.io) đấy, mẫu thì mình cũng lấy trên mạng luôn rồi sửa lại thôi và....Bùmmm!! Thành cái blog bạn đang đọc đây. Quả là ngon, bổ, miễn phí :v

<center> 
 {%  img img-align-center /img/Blog-Cua-Minh/easy-work.png 700 700 '"easy image" "Credit: thecontextofthings.com"'  %} 
</center>


# Blog mình viết về chủ đề nào ??

Everything !!

Bất kỳ chủ đề nào mà mình có thể viết và muốn viết thì mình sẽ viết. Không có một giới hạn nào về chủ đề của blog mình cả. Nó có thể là về tâm lý, về khoa học, về đời sống hằng ngày hay thậm chí là tình cảm (cái này thì tạm thời không viết nha :v) nên trang của mình có tên là **Thích Là Viết**. Mình đơn giản là viết những gì mình thích thôi.

{% blockquote Nicola Yoon, 'Everything, Everything' %}
    Just because you can’t experience everything doesn’t mean you shouldn’t experience anything.
{% endblockquote %}

# Túm lại

Nếu có thể, hãy cùng mình viết một trang blog cho riêng bản thân bạn và bạn sẽ học được rất nhiều thứ đấy. Để có một cái blog không phải là điều khó khăn vì bây giờ mọi công cụ hỗ trợ đã có sẵn hết rồi, chỉ cần bạn "gật đầu" là sẽ có ngay 1 cái blog ngon lành thôi nha. Chia sẻ những điều tuyệt vời để thấy được những điều tuyệt vời :3

Nếu bạn muốn làm blog những không biết làm như thế nào, bạn có thể liên lạc mình để mình hỗ trợ bạn. Chúc bạn luôn vui vẻ và những lúc rảnh rỗi thì vào đọc blog của mình nha.

Thông tin về mình [tại đây](/about).

<center> 
 {%  img img-align-center /img/Blog-Cua-Minh/cat.gif 500 500 '"cat"'  %} 
</center>



