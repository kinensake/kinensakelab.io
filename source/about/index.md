---
title: About Me
date: 2021-06-06 15:43:21
---

Xin chào bạn, thật là vui và vinh hạnh vì được bạn ghé thăm. Để mình giới thiệu sơ qua về bản thân của mình nhé :3

<center>
    {%  img /about/img/welcome.jpg 500 500 '"Welcome to my blog"'  %}
</center>

# Giới thiệu
Minh là Minh Quân (có lần đặt nhầm tên facebook nên bạn bè cứ gọi là Quân Quân :v). Nay mình đã 21 tuổi rồi và hiện đang là sinh viên tại trường Đại học Khoa học Tự nhiên chuyên ngành Computer Science. Nghe ngầu ngầu đúng hông, thật ra mình chỉ là một sinh viên quèn thôi à. Ngoài ra mình là một đứa rất *"yêu màu hường và ghét sự giả dối"*, chính vì thế nên hãy dành sự yêu thương cho mình nhé. Luv u <3

---
**Thông tin cá nhân của mình**

- **Họ và tên:** Đinh Nguyễn Minh Quân
- **Ngày sinh:** ngày 29 tháng 3 năm 2000
- **Nghề nghiệp:** sinh viên
- **Sở thích:** coi phim, đọc blog, lập trình,...
- **Số điện thoại:** 0946591640
- **Email:** quan2312016vn@gmail.com
- **Facebook:** https://facebook.com/sake.25
---

Chúc bạn có một ngày thật là vui vẻ. Nhớ xem blog của mình nếu rảnh nhé <3

<center> 
 {%  img /about/img/cat.gif 500 500 '"A cute cat"'  %} 
</center>

